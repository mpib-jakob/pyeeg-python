import sqlalchemy
import io
from pyeeg.io.external_tools.aep_gui.aep_matlab_tools import *
from pyeeg.processing.tools.peak_detection.time_domain_tools import EegPeak
from pyeeg.definitions.eegReaderAbstractClasses import EegAverageEpochs
import pandas as pd
from pandas.io import sql
import sqlite3
from sqlalchemy import create_engine
__author__ = 'jundurraga-ucl'


class SubjectInformation(object):
    def __init__(self, subject_id: str = '',
                 gender: str = '',
                 age: float = None,
                 date_of_birth: str = '',
                 handedness: str = '',
                 comments: str = ''):
        self.subject_id = subject_id
        self.gender = gender
        self.age = age
        self.date_of_birth = date_of_birth
        self.handedness = handedness
        self.comments = comments


class MeasurementInformation(object):
    def __init__(self, experiment: str = '', condition: str = '', date: str = '', comments: str = ''):
        self.experiment = experiment
        self.condition = condition
        self.date = date
        self.comments = comments


class PandasDataTable(object):
    def __init__(self, table_name: str = '', pandas_df: pd.DataFrame = None):
        self.table_name = table_name
        self.pandas_df = pandas_df


def store_data(data_base_path='',
               subject_info: SubjectInformation = None,
               measurement_info: MeasurementInformation = MeasurementInformation(),
               stimuli_info: dict = None,
               recording_info: dict = None,
               pandas_df: [PandasDataTable] = None,
               replace_matched_pandas_data: bool = True
               ):
    # connect and add items to data base
    # db = sqlite3.connect(data_base_path)
    db = create_engine("sqlite:///" + data_base_path)
    cnn = db.connect()
    # cursor = db.cursor()
    statement = "SELECT name FROM sqlite_master WHERE type='table';"
    print('Storing data in %s' % data_base_path)
    all_tables = cnn.execute(statement).fetchall()
    if ('subjects',) in all_tables:
        cursor = db.execute('select id from subjects ;')
        _index = len(cursor.fetchall()) + 1
    else:
        _index = 1
    _new_df = dict({'anonymous_name': 'S' + str(_index)}, **subject_info.__dict__)

    _id_subject = upsert_db(table_name='subjects', new_df=pd.DataFrame([_new_df]), db=db,
                            column_names=['subject_id'], replace=False)

    # add measurement info
    _row_measurement_info = dict({'id_subject': _id_subject},
                                 **measurement_info.__dict__)
    _id_measurement = upsert_db(table_name='measurement_info', new_df=pd.DataFrame([_row_measurement_info]),
                                db=db,
                                column_names=list(_row_measurement_info.keys()), replace=False)

    # add stimuli to table
    _row_stimuli = dict({'id_subject': _id_subject, 'id_measurement': _id_measurement}, **stimuli_info)
    # check no dict is passed
    [_row_stimuli.pop(_key) for _key in list(_row_stimuli.keys()) if isinstance(_row_stimuli[_key], dict)]
    _id_stimuli = upsert_db(table_name='stimuli', new_df=pd.DataFrame([_row_stimuli]), db=db,
                            column_names=list(_row_stimuli.keys()), replace=False)

    # add recording processing to table
    _row_recording = dict({'id_stimuli': _id_stimuli, 'id_subject': _id_subject, 'id_measurement': _id_measurement},
                          **recording_info)
    _id_recording = upsert_db(table_name='recording', new_df=pd.DataFrame([_row_recording]), db=db,
                              column_names=list(_row_recording.keys()), replace=False)

    for _df in pandas_df:
        _df.pandas_df['id_subject'] = _id_subject
        _df.pandas_df['id_measurement'] = _id_measurement
        _df.pandas_df['id_stimuli'] = _id_stimuli
        _df.pandas_df['id_recording'] = _id_recording
        upsert_db(table_name=_df.table_name, new_df=_df.pandas_df, db=db,
                  column_names=['id_subject', 'id_measurement', 'id_stimuli', 'id_recording'],
                  replace=replace_matched_pandas_data)


def find_item_in_table(table_name='', new_df=pd.DataFrame,db=sqlite3.Connection):
    cursor = db.cursor()
    statement = "SELECT name FROM sqlite_master WHERE type='table';"
    all_tables = cursor.execute(statement).fetchall()
    if (table_name,) in all_tables:
        table = sql.read_sql('select * from ' + table_name, db)
        _found = table[new_df.columns].isin(new_df)


def upsert_db(table_name='', new_df=pd.DataFrame, column_names: [str] = None, db=sqlite3.Connection, replace=True):
    if not new_df.size:
        return None
    statement = "SELECT name FROM sqlite_master WHERE type='table';"
    all_tables = db.execute(statement).fetchall()
    new_df.to_sql('my_tmp_table', db, if_exists='replace', index=True, index_label='id')
    connection = db.raw_connection()
    cursor = connection.cursor()
    if (table_name,) in all_tables:
        # delete those rows that we are going to "upsert"
        _condition = ' and '.join(
            ['{:s} IS {:s}'.format(_a, _b) for _a, _b in [(''.join(table_name + '.' + x),
                                                          ''.join('my_tmp_table.' + x)) for x in column_names]])
        table_matches = sql.read_sql('select id from ' + table_name +
                                     ' where exists (select * from my_tmp_table where ' +
                                      _condition + ')', db)
        if replace or len(table_matches) == 0:
            cursor.execute('delete from ' + table_name + ' where exists (select * from my_tmp_table where ' +
                           _condition + ')')
            connection.commit()
            # insert changed rows
            _table = cursor.execute('select id from ' + table_name + ' ;')
            _all_ids = _table.fetchall()
            idx_off_set = np.max(_all_ids) + 1 if _all_ids else 0
            _index = new_df.index.values + idx_off_set
            new_df['id'] = _index
            new_df.to_sql(table_name, db, if_exists='append', index=False)
        else:
            _index = table_matches.iloc[0]['id']
    else:
        new_df.to_sql(table_name, db, if_exists='append', index=True, index_label='id')
        # get new index from DB
        _id = sql.read_sql('select id from ' + table_name, db)
        _index = _id.values
    # delete temp table
    cursor.execute('DROP TABLE IF EXISTS my_tmp_table')
    connection.commit()
    cursor.close()
    return int(_index) if _index.size == 1 else _index
