from pyeeg.io.edf_bdf_reader import read_edf_bdf_header, get_data, get_event_channel, DeviceEventChannel
import os
import abc
import numpy as np


class EEGData(object, metaclass=abc.ABCMeta):
    def __init__(self, file_name=''):
        self.file_name = file_name
        self.fs = None
        self.n_channels = None
        self.default_layout = None
        self.event_channel_label = None

    def get_data(self, channels: np.array = np.array([]), ini_time=0, end_time: float = None):
        print('need to define')

    def get_events(self):
        print('need to define')


class _BDFEDFDataReader(EEGData):
    def __init__(self, file_name=''):
        super(_BDFEDFDataReader, self).__init__(file_name=file_name)
        self._header = read_edf_bdf_header(self.file_name)
        self.n_channels = self._header['n_channels']
        self.fs = self._header['fs'][0]
        self.default_layout = None
        self.data_units = None
        _, file_extension = os.path.splitext(file_name)
        if file_extension == '.bdf':
            self.event_channel_label = DeviceEventChannel.bdf_event_channel
        if file_extension == '.edf':
            self.event_channel_label = DeviceEventChannel.edf_event_channel

    def get_data(self, channels_idx: np.array = np.array([]), ini_time=0, end_time: float = None):
        data, events, units, annotations = get_data(header=self._header, channels_idx=channels_idx, ini_time=ini_time,
                                                    end_time=end_time,
                                                    event_channel_label=self.event_channel_label)
        self.data_units = units
        if channels_idx.size:
            self.default_layout = self._header['channels'][channels_idx]
        else:
            self.default_layout = self._header['channels']

        # here the status channel is removed from the layout
        _status_idx = [_i for _i, _ch in enumerate(self.default_layout) if
                       self.default_layout[_i].label == self.event_channel_label]
        if _status_idx:
            self.default_layout = np.delete(self.default_layout, _status_idx)
        return data, events, units, annotations

    def get_events(self):
        _events, _ = get_event_channel(header=self._header,
                                       event_channel_label=self.event_channel_label)
        return _events


def eeg_reader(file_name=''):
    _, file_extension = os.path.splitext(file_name)
    if file_extension in ['.bdf', '.edf']:
        return _BDFEDFDataReader(file_name=file_name)
