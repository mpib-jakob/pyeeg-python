import pyeeg.io.synergy_reader as sr
import pyeeg.processing.tools.epochs_processing_tools as ap
import sqlalchemy
from os.path import sep
from scipy.optimize import curve_fit
import pyeeg.processing.tools.fitting.fitting_functions as ff
from os.path import abspath, dirname
from pyeeg.processing.tools.peak_detection.time_domain_tools import *
import dataset
import io
import matplotlib.gridspec as gridspec
__author__ = 'jundurraga'


def get_eabr_synergy(**kwargs):
    file_path = kwargs.get('file_path', '')
    data_base_path = kwargs.get('data_base_path', '')
    save_to_data_base = kwargs.get('save_to_data_base', False)
    experiment = kwargs.get('experiment', 'eabr')
    save_single_recordings_to_db = kwargs.get('save_single_recordings_to_db', True)
    peak_to_peak_amp_labels = kwargs.get('peak_to_peak_amp_labels', [{}])
    fitting_ini_time = kwargs.get('fitting_ini_time', 0.0008)
    fitting_end_time = kwargs.get('fitting_end_time', 0.009)
    ini_expected_peaks = kwargs.get('ini_expected_peaks', None)
    time_windows = kwargs.get('time_windows', None)
    amplifier_gain = kwargs.get('amplifier_gain', 100.0)

    data_path = dirname(file_path)
    out_path = abspath(file_path).split('.')[0] + '.ini'
    input_codec = 'UTF-16'
    output_codec = 'ASCII'
    unicode_file = open(file_path, "rb")
    out_file = open(out_path, "wb")
    unicode_data = unicode_file.read().decode(input_codec)
    out_byte_string = unicode_data.encode(output_codec, 'ignore')
    out_file.write(out_byte_string)
    unicode_file.close()
    out_file.close()

    reader = sr.SynergyReader(file_name=out_path)
    reader.gain = -amplifier_gain / 1000.0
    reader.time_offset = 0.0
    ini_fit = reader.time_to_samples(fitting_ini_time)
    end_fit = reader.time_to_samples(fitting_end_time)

    # fit exponential
    fitting_coefficients = []
    _corrected_data = np.zeros(reader.data.shape)
    for _idx in np.arange(reader.data.shape[1]):
        a0 = 0
        a1 = np.mean(reader.data[ini_fit: ini_fit + (end_fit - ini_fit) / 5, _idx])
        a22 = np.maximum((np.mean(reader.data[end_fit - (end_fit - ini_fit) / 5: end_fit, _idx]) - a0) / a1, 1e-10)
        a2 = -np.log(a22) / (reader.time[end_fit] - reader.time[ini_fit])
        p0 = [a0, a1, a2]
        try:
            popt, pcov = curve_fit(ff.electrical_exponential_decay, reader.time[ini_fit:end_fit],
                                   np.squeeze(reader.data[ini_fit:end_fit, _idx]), p0=p0)
        except RuntimeError:
            popt = p0

        _corrected_data[:, _idx] = reader.data[:, _idx] - ff.electrical_exponential_decay(reader.time, *popt)
        _corrected_data[0:ini_fit, _idx] = _corrected_data[ini_fit, _idx]
        fitting_coefficients.append(popt)
    # _corrected_data = _corrected_data[:, 6:9]
    snr, rn = ap.estimate_srn_from_trace(data=_corrected_data, snr_ini_sample=reader.time_to_samples(0.002),
                                         rn_ini_sample=reader.time_to_samples(0.007))

    # initial peaks estimation
    peaks, _mean_peaks = get_abr_peaks(time_vector=reader.time,
                                       data_channels=_corrected_data,
                                       rn=rn,
                                       snr=snr,
                                       ignore_below=fitting_ini_time * 1.1,
                                       expected_peaks=ini_expected_peaks,
                                       **kwargs)

    amplitudes = measure_peak_to_peak_amplitudes(detected_peaks=peaks, peak_to_peak_amp_labels=peak_to_peak_amp_labels)
    # get information from parameters
    subject = reader.subject['family name']
    _title = subject
    offset_step = np.max(np.abs(np.max(reader.data[ini_fit:end_fit, :], axis=0) -
                                np.min(reader.data[ini_fit:end_fit, :], axis=0)))
    offset_vector = -np.arange(_corrected_data.shape[1]) * offset_step
    font = {'size': 8}
    fig, ax1 = plt.subplots()
    ax1.set_ylim([offset_vector[-1] - offset_step, offset_vector[0] + offset_step])
    plt.rc('font', **font)
    for _idx, _ch_peak in enumerate(peaks):
        ax1.plot(reader.time, reader.data[:, _idx] + offset_vector[_idx], 'b')
        ax1.plot(reader.time[ini_fit::],
                 ff.electrical_exponential_decay(reader.time[ini_fit::],
                                                 *fitting_coefficients[_idx]) + offset_vector[_idx], 'g')
        ax1.plot(reader.time, _corrected_data[:, _idx] + offset_vector[_idx], 'r')
        ax1.legend(['original', 'exponential decay', 'corrected'], loc='lower right')
        # add detected peaks
        for _peak in _ch_peak:
            if not _peak.label_peak:
                continue
            if _peak.positive:
                ax1.plot(_peak.time, _peak.amp + offset_vector[_idx], '^', markersize=2)
                if _peak.show_label:
                    ax1.text(_peak.time, _peak.amp * 1.1 + offset_vector[_idx], _peak.label_peak)
            else:
                ax1.plot(_peak.time, _peak.amp + offset_vector[_idx], 'v', markersize=2)
                if _peak.show_label:
                    ax1.text(_peak.time, _peak.amp * 1.1 + offset_vector[_idx], _peak.label_peak, size=4)

    if time_windows:
        [ax1.axvspan(_t_w.ini_time, _t_w.end_time, alpha=0.15, color='r')
         for _t_w in time_windows if _t_w.positive_peak]
        [ax1.axvspan(_t_w.ini_time, _t_w.end_time, alpha=0.15, color='b')
         for _t_w in time_windows if not _t_w.positive_peak]

    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel('Amplitude [${\mu}V$]')
    ax1.set_title(_title)
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(offset_vector)
    ax2.set_yticklabels([str(_ch) for _ch in reader.channelLabels])
    ax2.spines["right"].set_position(("axes", - 0.1))

    _name_parameters = 'eabr_' + subject
    fig.savefig(data_path + sep + _name_parameters + '.svg')
    plt.close(fig)

    # plot comparison between corrected and uncorrected data
    ig = plt.figure()
    gs = gridspec.GridSpec(1, 2)
    ax = plt.subplot(gs[0, 0])
    ax.plot(reader.time, reader.data)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Amplitude [${\mu}V$]')
    ax.set_title('Original')
    ax = plt.subplot(gs[0, 1])
    ax.plot(reader.time, _corrected_data)
    ax.set_title('Corrected')
    ax.set_xlabel('Time [s]')
    ig.savefig(data_path + sep + _name_parameters + '_comparison.svg')
    ig.clf()

    if not save_to_data_base:
        return
    #########################################################################
    # add all to data base #######################################
    _channels = [{'channel': _ch} for _ch in reader.channelLabels]
    _data_rec_params = join_arr_dict_to_arr_dict(arr_1=reader.channel_recording_parameters,
                                                 arr_2=_channels)
    _data_time_peaks = join_to_channel_dict(dict_to_join=peaks,
                                            channels=_channels)
    _amplitude_table = join_to_channel_dict(dict_to_join=amplitudes, channels=_channels)

    # connect and add items to data base
    db = dataset.connect('sqlite:///' + data_base_path)
    table_subjects = db['subjects']
    table_subjects.create_column('name', sqlalchemy.String)
    table_subjects.create_column('anonymous_name', sqlalchemy.String)
    # check if subject is in DB
    result = table_subjects.find_one(name=subject)
    if not result:
        _id_subject = table_subjects.insert({'name': subject})
        table_subjects.update(dict(id=_id_subject, anonymous_name='S' + str(_id_subject)), ['id'])
    else:
        print('adding data to subject found in DB')
        _id_subject = result['id']
    # add experiment to table
    table_experiment = db['experiment']
    exp = {'type': experiment, 'id_subject': _id_subject}
    table_experiment.upsert(exp, list(exp.keys()))
    _id_experiment = table_experiment.find_one(type=experiment)['id']

    # add measurement info
    table_measurement_info = db['measurement_info']
    table_measurement_info.create_column('id_experiment', sqlalchemy.INTEGER)

    _row_measurement_info = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject},
                                 **reader.subject)
    table_measurement_info.upsert(_row_measurement_info, list(_row_measurement_info.keys()))

    # add amplitudes
    table_recording_parameters = db['recording_parameters']
    for _, item in enumerate(_data_rec_params):
        _row_test = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject}, **item)
        table_recording_parameters.upsert(_row_test, list(_row_test.keys()))

    # add time peaks
    table_time_peaks = db['time_peaks']
    for _, item in enumerate(_data_time_peaks):
        if item['label_peak'] == 'NA':
            continue
        _row_test = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject}, **item)
        table_time_peaks.upsert(_row_test, list(_row_test.keys()))

    # add amplitudes
    table_amplitude = db['amplitudes']
    for _, item in enumerate(_amplitude_table):
        if item['label_amp'] == 'NA':
            continue
        _row_test = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject}, **item)
        table_amplitude.upsert(_row_test, list(_row_test.keys()))

    # add time data
    table_responses = db['responses']
    table_responses.create_column('domain', sqlalchemy.String)
    table_responses.create_column('type', sqlalchemy.String)
    table_responses.create_column('channel', sqlalchemy.String)
    table_responses.create_column('x', sqlalchemy.LargeBinary)
    table_responses.create_column('y', sqlalchemy.LargeBinary)
    table_responses.create_column('snr', sqlalchemy.Float)
    # save best channels spectra and wave forms to db

    # save individual channels if requested
    if save_single_recordings_to_db:
        for i in range(_corrected_data.shape[1]):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, reader.time)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, _corrected_data[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'id_experiment': _id_experiment, 'id_subject': _id_subject,
                              'domain': 'time', 'type': 'average',
                              'channel': reader.channelLabels[i],
                              'x': data_binary_x.read(), 'y': data_binary_y.read(),
                              'snr': snr[i]}
            table_responses.upsert(_row_time_data, list(_row_time_data.keys()))
