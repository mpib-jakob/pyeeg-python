from pyeeg.definitions.eegReaderAbstractClasses import *
from pyeeg.plot import eeg_ave_epochs_plot_tools as eegpt
import re
from pyeeg.io.external_tools.aep_gui.aep_matlab_tools import *
from pyeeg.definitions.eeg_definitions import Domain
__author__ = 'jaime undurraga'


def get_time_ep(data_reader=EEGData, **kwargs):
    low_pass = kwargs.pop('low_pass', None)
    high_pass = kwargs.pop('high_pass', None)
    notch_filter = kwargs.pop('notch_frequencies', None)
    amplitude_unit = kwargs.get('amplitude_unit', 'microvolt')
    time_unit = kwargs.get('time_unit', 'second')
    peak_time_windows = kwargs.pop('peak_time_windows', [])
    peak_to_peak_amp_labels = kwargs.pop('peak_to_peak_amp_labels', [])
    peak_detection_sampling_factor = kwargs.pop('peak_detection_sampling_factor', 1.0)
    eeg_topographic_map_channels = kwargs.get('eeg_topographic_map_channels', None)
    oeg_channels = kwargs.pop('oeg_channels', [])
    bad_channels = kwargs.pop('bad_channels', [])
    ref_channels = kwargs.pop('ref_channels', [])
    sf_active = kwargs.pop('sf_active', True)
    sf_components = kwargs.get('sf_components', np.array([]))
    sf_plot_components = kwargs.get('sf_plot_components', True)
    sf_join_frequencies = kwargs.get('sf_join_frequencies', None)
    test_frequencies = kwargs.get('test_frequencies', None)
    invert_polarity = kwargs.get('invert_polarity', True)
    epoch_max_length = kwargs.get('epoch_max_length', None)
    roi_windows = kwargs.get('roi_windows', None)
    plot_time_x_lim = kwargs.get('plot_time_x_lim', None)
    plot_freq_x_lim = kwargs.get('plot_freq_x_lim', None)
    plot_time_y_lim = kwargs.get('plot_time_y_lim', None)
    plot_freq_y_lim = kwargs.get('plot_freq_y_lim', None)
    title = kwargs.get('title', '')
    subset_identifier = kwargs.get('subset_identifier', '')
    average_domain = kwargs.get('average_domain', Domain.time)
    amp_sigma_thr = kwargs.get('amp_sigma_thr', 2.0)
    triggers = kwargs.pop('triggers', {})
    figure_label = kwargs.get('figure_label', '')
    sf_thr = kwargs.get('sf_thr', 0.8)
    detect_abr_peaks = kwargs.get('detect_abr_peaks', False)
    n_epochs_to_read = kwargs.get('n_epochs_to_read', None)
    ini_epoch_to_read = kwargs.get('ini_epoch_to_read', None)
    time_offset = kwargs.get('time_offset', 0.0) # specify time offset in sec
    fig_format = kwargs.get('fig_format', '.png')
    ref_channel_peak_detection = kwargs.get('ref_channel_peak_detection', None)
    user_average_channels = kwargs.get('user_average_channels', [])
    fontsize = kwargs.get('fontsize', 10)
    interpolation_rate = kwargs.get('interpolation_rate', None)
    interpolation_width = kwargs.get('interpolation_width', None)
    interpolation_offset = kwargs.get('interpolation_offset', 0)
    trigger_factor = kwargs.get('trigger_factor', None)
    plot_time_responses = kwargs.get('plot_time_responses', True)
    plot_frequency_responses = kwargs.get('plot_frequency_responses', True)
    plot_time_frequency_responses = kwargs.get('plot_time_frequency_responses', False)
    time_frequency_transformation_parameters = kwargs.get('time_frequency_transformation_parameters', {})

    """ Compute and save time average responses into a database.
    Keyword arguments:
    stimulus_parameters -- dictionary list containing measurement parameters
    channels -- dictionary list containing eeg channels information
    low_pass -- low-pass cutoff frequency (it must be a float)
    high_pass -- high-pass cutoff frequency (it must be a float)
    generate_mat_file  -- if True, a matlab matrix will be saved in path
    save_to_data_base -- if True, data measurements will be stored in data base
    data_base_path -- path where database will be stored
    save_single_recordings_to_db -- if true, single channel data will be stored in database
    experiment -- string identifying the type of experiment
    notch_filter -- float list indicating the frequencies if the notch filter
    plot_title_parameters -- string list containing key names from stimulus_parameters to be added on the figure title
    interpolate_data -- data point will be linearly interpolated
    amplitude_unit -- unit to scale amplitude, it can be 'microvolt', 'millivolt', or emtpy
    time_unit -- unit to scale time, it can be 'microsecond', 'millisecond', or empty
    peak_windows -- dictionary containing the times used to classify peaks
    peak_to_peak_amp_labels -- which peaks are used to measure amplitudes
    """

    # set polarity (invert waveforms)
    data_reader.average_domain = average_domain
    data_reader.invert_polarity = invert_polarity
    # set subset identifier
    data_reader.paths.subset_identifier = subset_identifier
    # set number of epochs to read
    data_reader.n_epochs_to_read = n_epochs_to_read
    data_reader.ini_epoch_to_read = ini_epoch_to_read

    # set filter frequencies
    data_reader.low_pass_freq = low_pass
    data_reader.high_pass_freq = high_pass
    data_reader.notch_frequencies = notch_filter
    # set time offset
    data_reader.time_offset = time_offset
    # set roi windows
    data_reader.roi_windows = roi_windows
    # set reference channels
    data_reader.ref_channels = ref_channels
    # set known bad channels
    data_reader.bad_channels = bad_channels
    # set known oeg channels
    data_reader.oeg_channels = oeg_channels

    # get scalp data
    _d_bad_channels = data_reader.set_epochs_raw(epoch_length=epoch_max_length, triggers=triggers, **kwargs)

    # append manually selected channels
    _d_bad_channels = [_ch for _ch in set(bad_channels + _d_bad_channels)]
    # remove detected bad channels on entire data
    data_reader.remove_channels(channels_to_remove=_d_bad_channels, keep=data_reader.ref_channels)

    # set reference
    data_reader.set_reference()
    # reference data after removing bad channels
    data_reader.epochs_raw = data_reader.reference_epochs(epochs=data_reader.epochs_raw)

    # get eye channels and remove theirs artifacts from raw data
    data_reader.remove_oeg_artifacts(triggers=triggers,
                                     epoch_length=epoch_max_length,
                                     interpolation_rate=interpolation_rate,
                                     interpolation_width=interpolation_width,
                                     interpolation_offset=interpolation_offset,
                                     trigger_factor=trigger_factor)

    # compute raw average
    data_reader.set_epochs_raw_ave()

    # compute spatial filter and clean data and add
    if sf_active:
        data_reader.apply_spatial_filtering(sf_components=sf_components, sf_join_frequencies=sf_join_frequencies,
                                            sf_thr=sf_thr)
        # save components on spatial_filter
        _fig_path = data_reader.paths.figure_basename_path + figure_label + '_components' + fig_format
        eegpt.plot_time_single_channels(ave_data=data_reader.spatial_filter.components_ave,
                                        file_name=_fig_path,
                                        title=title,
                                        time_unit=time_unit,
                                        amplitude_unit='dimensionless',
                                        idx_ch_to_plot=list(range(11)),
                                        x_lim=plot_time_x_lim,
                                        fontsize=fontsize)
    else:
        data_reader.epochs_processed = data_reader.epochs_raw

    # compute averaged clean data
    data_reader.set_epochs_processed_ave(test_frequencies=test_frequencies)

    # compute and append across channels average
    data_reader.append_average_channel(data_reader.epochs_processed_ave)

    # compute other averege channels
    for u_ch in user_average_channels:
        data_reader.append_average_channel(data_reader.epochs_processed_ave, channel_labels=u_ch)

    # compute and append global field power
    data_reader.append_gfp_channel(data_reader.epochs_processed_ave)

    # resampling data to improve peak accuracy in time domain
    data_reader.epochs_processed_ave.resampling(new_fs=data_reader.fs * peak_detection_sampling_factor)

    # get index with maximum snr physical channels to use as reference
    _ch_idx = data_reader.epochs_processed_ave.is_scalp_channel()
    max_snr_idx = np.argmax(data_reader.epochs_processed_ave.get_max_snr_per_channel()[_ch_idx])
    if not ref_channel_peak_detection:
        ref_channel_peak_detection = data_reader.epochs_processed_ave.channels[max_snr_idx].label
    # find peaks and amplitudes for averaged channels
    data_reader.set_peaks_and_amplitudes(averaged_epochs=data_reader.epochs_processed_ave,
                                         peak_time_windows=peak_time_windows,
                                         peak_to_peak_measures=peak_to_peak_amp_labels,
                                         amp_sigma_thr=amp_sigma_thr,
                                         detect_abr_peaks=detect_abr_peaks,
                                         ref_channel_peak_detection=ref_channel_peak_detection
                                         )

    #  plot and save time waveforms and time peaks
    _name_parameters = re.sub('\.', '_', '_fs_' + str(round(data_reader.epochs_processed_ave.fs)) +
                              '_hi_' + str(high_pass) +
                              '_low_' + str(low_pass))
    if sf_active:
        _name_parameters += re.sub('\.', '_', '_SFC_' +
                                   str(data_reader.spatial_filter.component_indexes[0]) +
                                   '_' +
                                   str(data_reader.spatial_filter.component_indexes[-1])
                                   )
    _fig_path = data_reader.paths.figure_basename_path + figure_label + _name_parameters + '_ave_' + fig_format

    eegpt.plot_time_single_channels(ave_data=data_reader.epochs_processed_ave,
                                    file_name=_fig_path,
                                    title=title,
                                    time_unit=time_unit,
                                    amplitude_unit=amplitude_unit,
                                    x_lim=plot_time_x_lim,
                                    fontsize=fontsize)

    # include special topographic maps
    for _tm in eeg_topographic_map_channels:
        if _tm == 'max_snr':
            eeg_topographic_map_channels = np.append(eeg_topographic_map_channels,
                                                     data_reader.epochs_processed_ave.channels[_ch_idx][
                                                         max_snr_idx].label)
        if _tm == 'all_channels':
            _is_scalp_ch = data_reader.epochs_processed_ave.is_scalp_channel()
            eeg_topographic_map_channels = [data_reader.epochs_processed_ave.channels[_idx].label for
                                            _idx, _is in enumerate(_is_scalp_ch) if _is]

    if sf_active and sf_plot_components:
        # plot individual components
        data_reader.set_individuals_components_maps(n_components=10, test_frequencies=test_frequencies)
        data_reader.plot_individuals_components(peak_time_windows=peak_time_windows,
                                                eeg_topographic_map_channels=None,
                                                time_unit=time_unit,
                                                amplitude_unit=amplitude_unit,
                                                component_maps=data_reader.component_maps_ave,
                                                x_lim_time=plot_time_x_lim,
                                                y_lim_time=plot_time_y_lim,
                                                x_lim_freq=plot_freq_x_lim,
                                                y_lim_freq=plot_freq_y_lim,
                                                fig_format=fig_format,
                                                fontsize=fontsize,
                                                domain=average_domain)

    # plot and save frequency waveforms and peaks
    _fig_path = data_reader.paths.figure_basename_path + figure_label + _name_parameters + '_ave_freq' + fig_format
    eegpt.plot_freq_single_channels(ave_data=data_reader.epochs_processed_ave,
                                    file_name=_fig_path,
                                    title=title,
                                    time_unit=time_unit,
                                    amplitude_unit=amplitude_unit,
                                    x_lim=plot_freq_x_lim)

    # plot time potential fields for average channel
    if plot_time_responses:
        eegpt.plot_eeg_topographic_map(ave_data=data_reader.epochs_processed_ave,
                                       eeg_topographic_map_channels=eeg_topographic_map_channels,
                                       figure_dir_path=data_reader.paths.figure_basename_path,
                                       figure_basename=figure_label + _name_parameters,
                                       time_unit=time_unit,
                                       amplitude_unit=amplitude_unit,
                                       domain=Domain.time,
                                       title=title,
                                       x_lim=plot_time_x_lim,
                                       y_lim=plot_time_y_lim,
                                       fig_format=fig_format,
                                       fontsize=fontsize)
    if plot_time_frequency_responses:
        eegpt.plot_eeg_time_frequency_transformation(ave_data=data_reader.epochs_processed_ave,
                                                     eeg_topographic_map_channels=eeg_topographic_map_channels,
                                                     figure_dir_path=data_reader.paths.figure_basename_path,
                                                     figure_basename=figure_label + _name_parameters,
                                                     time_unit=time_unit,
                                                     amplitude_unit=amplitude_unit,
                                                     domain=Domain.time,
                                                     title=title,
                                                     x_lim=plot_time_x_lim,
                                                     y_lim=plot_freq_x_lim,
                                                     fig_format=fig_format,
                                                     fontsize=fontsize,
                                                     **time_frequency_transformation_parameters)

    # plot frequency potential fields for average channel
    if plot_frequency_responses:
        eegpt.plot_eeg_topographic_map(ave_data=data_reader.epochs_processed_ave,
                                       eeg_topographic_map_channels=eeg_topographic_map_channels,
                                       figure_dir_path=data_reader.paths.figure_basename_path,
                                       figure_basename=figure_label + _name_parameters,
                                       time_unit=time_unit,
                                       amplitude_unit=amplitude_unit,
                                       domain=Domain.frequency,
                                       title=title,
                                       x_lim=plot_freq_x_lim,
                                       y_lim=plot_freq_y_lim,
                                       fig_format=fig_format,
                                       fontsize=fontsize)
