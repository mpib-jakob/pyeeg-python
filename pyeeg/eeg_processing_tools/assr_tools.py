import matplotlib.pyplot as plt
import pyeeg.processing.tools.filters.spatial_filtering as sf
import pyeeg.plot.eeg_plot_tools as eegpt
from os.path import sep
import pyeeg.readingEEGData.biosemiReader as bioReader
import pyeeg.biosemi_tools.biosemiLayouts as bioLayout
import dataset
import sqlalchemy
import io
import scipy.io
import re
from pyeeg.io.external_tools.aep_gui.aep_matlab_tools import *
__author__ = 'jundurraga-ucl'


def get_assr(bdf_reader=bioReader.BiosemiDataReader, **kwargs):
    stimulus_parameters = kwargs.get('stimulus_parameters', [])
    measurement_info = kwargs.get('measurement_info', {'Comments': '', 'Condition': '', 'Date':'', 'Subject': 'NN'})
    channels = kwargs.get('channels', {})
    data_name = kwargs.get('data_name', '')
    path = kwargs.get('path', '')
    low_pass = kwargs.get('low_pass', -1)
    high_pass = kwargs.get('high_pass', -1)
    generate_mat_file = kwargs.get('generate_mat_file', False)
    save_to_data_base = kwargs.get('save_to_data_base', False)
    data_base_path = kwargs.get('data_base_path', '')
    save_single_recordings_to_db = kwargs.get('save_single_recordings_to_db', False)
    experiment = kwargs.get('experiment', 'assr')
    notch_filter = kwargs.get('notch_filter', [50.0, 100.0, 150.0, 200.0, 250.0, 300.0])
    sf_freq = kwargs.get('sf_freq', True)
    sf_join_frequencies = kwargs.get('sf_join_frequencies', None)
    layout = kwargs.get('layout', bioLayout.get_64_channels_layout())
    test_frequencies = get_frequencies_of_interest(stimulus_parameters=stimulus_parameters)
    save_components = kwargs.get('save_components', True)
    n_components_plot = kwargs.get('n_components_plot', 10)
    plot_title_parameters = kwargs.get('plot_title_parameters', [])

    if not sf_join_frequencies:
        sf_join_frequencies = test_frequencies

    #  compute steady response for selected frequencies
    j_ave, epochs, n_components, components = get_biosemi_assr(bdf_reader=bdf_reader,
                                                               test_frequencies=test_frequencies,
                                                               sf_join_frequencies=sf_join_frequencies,
                                                               **kwargs)
    if generate_mat_file:
        _file_name = path + sep + data_name + '.mat'
        scipy.io.savemat(_file_name, {'epochs': epochs})

    # plot averaged channels
    font = {'size': 6}
    # find best channels for a given frequency bin
    h_t_square_test = j_ave.hotelling_t_square_test()

    bin_mod_freq = np.where(j_ave.frequencies_to_analyze == test_frequencies[0])[0]

    pos_max_snr_mf = np.argmax(np.array([item[bin_mod_freq]['snr'] for i, item in enumerate(h_t_square_test)]))

    plt.rc('font', **font)
    ave_s_fft_all_splits = 2.0 / j_ave._win_size * j_ave.s_fft_spectral_ave_all_splits
    ave_w_fft_all_splits = 2.0 / j_ave._win_size * j_ave.w_fft_spectral_ave_all_splits
    ave_w_fft_bin_all_splits = 2.0 / j_ave._win_size * j_ave.w_fft_spectral_weighted_ave_all_splits(0)
    fig, ax1 = plt.subplots()
    ax1.plot(j_ave._fft_frequencies,
             np.vstack((ave_s_fft_all_splits,
             ave_w_fft_all_splits,
             ave_w_fft_bin_all_splits)).T)
    # plot best channels (as defined by their snr)
    ax1.plot(j_ave._fft_frequencies, 2.0 / j_ave._win_size * np.abs(j_ave.s_fft[:, pos_max_snr_mf]))
    ax1.plot(j_ave._fft_frequencies, 2.0 / j_ave._win_size * np.abs(j_ave.w_fft[:, pos_max_snr_mf]))

    ax1.plot(j_ave._fft_frequencies[j_ave._frequency_bin],
             np.vstack((ave_s_fft_all_splits[j_ave._frequency_bin],
             ave_w_fft_all_splits[j_ave._frequency_bin],
             ave_w_fft_bin_all_splits[j_ave._frequency_bin])).T,
             marker='v',
             linestyle='None',
             markerfacecolor='black',
             markersize=2.0)

    ax1.legend(['standard', 'weighted entire spectrum', 'wighted at bin',
                'best_assr_s' + channels[pos_max_snr_mf]['label'],
                'best_assr_w' + channels[pos_max_snr_mf]['label'],
                ])

    title_params = []
    for _stimulus in stimulus_parameters:
        title_params.append(['/'] + [item + ':' + '{:10.3f}'.format(_stimulus[item]).strip() +
                                     '/' for i, item in enumerate(plot_title_parameters)])
    _title = measurement_info['Subject']
    for _title_par in title_params:
        _title += '\n'.join([''.join(_title_par)])
    ax1.set_title(_title)
    ax1.set_xlim(0, np.max(j_ave.frequencies_to_analyze) * 1.2)
    ax1.set_ylim(0, 1)
    ax1.set_xlabel('Frequency [Hz]')
    ax1.set_ylabel('Amplitude [' + r'$\mu$' + 'V]')
    fig.savefig(path + sep + data_name + '_ave_channels' +
                '_Hi_' + re.sub('\.', '_', str(high_pass)) +
                '_Low_' + re.sub('\.', '_', str(low_pass)) +
                '_SF_' + re.sub('\.', '_', str(sf_freq)) +
                '_SFJF_' + re.sub('[\.\[\]\]]', '_',
                                  str(np.round(sf_join_frequencies, decimals=1)).replace('[ ', '').replace(' ', '_')) +
                '_SFNC_' + str(n_components) +
                '.svg')
    plt.close(fig)

    # plot time domain response
    fig, ax1 = plt.subplots()
    ax1.plot(j_ave.time, j_ave.w_average[:, pos_max_snr_mf])
    ax1.set_title(_title)
    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel('Amplitude [' + r'$\mu$' + 'V]')
    fig.savefig(path + sep + data_name + '_best_channel' +
                '_Hi_' + re.sub('\.', '_', str(high_pass)) +
                '_Low_' + re.sub('\.', '_', str(low_pass)) +
                '_SF_' + re.sub('\.', '_', str(sf_freq)) +
                '_SFJF_' + re.sub('[\.\[\]\]]', '_',
                                  str(np.round(sf_join_frequencies, decimals=1)).replace('[ ', '').replace(' ', '_')) +
                '_SFNC_' + str(n_components) +
                '_time_' +
                '.svg')
    plt.close(fig)

    # plot individual channels
    offset_step = 0.1
    offset_vector = np.arange(j_ave.splits) * offset_step
    ave_s_fft = 2.0 / j_ave._win_size * np.abs(j_ave.s_fft) - offset_vector.reshape(1, -1)
    ave_w_fft = 2.0 / j_ave._win_size * np.abs(j_ave.w_fft) - offset_vector.reshape(1, -1)

    fig, ax1 = plt.subplots()
    p1 = ax1.plot(j_ave._fft_frequencies, ave_s_fft, color='b', linewidth=0.5)
    p2 = ax1.plot(j_ave._fft_frequencies, ave_w_fft, color='g', linewidth=0.5)

    plt.legend([p1[0], p2[0]], ['s_ave', 'w_ave'])

    h_t_square_split = []
    p_values = []
    for i, h_test in enumerate(h_t_square_test):
        h_t_square_split.append(str(['f:' + '{:10.1f}'.format(freq_test['frequency_tested']) +
                                     '/p:' + '{:10.2f}'.format(freq_test['p_value'])
                                     for j, freq_test in enumerate(h_test)]))
        p_values.append([freq_test['p_value'] for j, freq_test in enumerate(h_test)])

    for i in range(j_ave.splits):
        for j, f_bin in enumerate(j_ave._frequency_bin):
            ax1.plot(j_ave._fft_frequencies[f_bin], ave_s_fft[f_bin, i],
                     marker='v',
                     linestyle='None',
                     markerfacecolor='black' if p_values[i][j] < 0.05 else 'white',
                     markersize=2.0)
            ax1.plot(j_ave._fft_frequencies[f_bin], ave_w_fft[f_bin, i],
                     marker='v',
                     linestyle='None',
                     markerfacecolor='black' if p_values[i][j] < 0.05 else 'white',
                     markersize=2.0)
    ax1.set_xlabel('Frequency [Hz]')
    ax1.set_ylabel('Amplitude [' + r'$\mu$' + 'V]')
    ax1.set_title(_title)
    ax1.set_xlim(0, np.max(j_ave.frequencies_to_analyze) * 1.2)
    ax1.set_ylim(-j_ave.splits*offset_step, 1)
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(-offset_vector)
    ax2.set_yticklabels([ch['label'] for i, ch in enumerate(channels)])
    ax2.spines["right"].set_position(("axes", - 0.1))
    _name_parameters = ('_Hi_' + re.sub('\.', '_', str(high_pass)) + '_Low_' + re.sub('\.', '_', str(low_pass)) +
                        '_SF_' + re.sub('\.', '_', str(sf_freq)) +
                        '_SFJF_' + re.sub('[\.\[\]\,\s]', '_',
                                          str(np.round(sf_join_frequencies, decimals=1)).replace('[ ', '').replace(' ', '_')) +
                        '_SFNC_' + re.sub('\.', '_', str(n_components)))
    fig.savefig(path + sep + data_name + '_single_channels' +_name_parameters + '.svg')
    plt.close(fig)

    # plot components
    if save_components:
        fig, ax1 = plt.subplots()
        c_offset_step = 0.0001
        c_offset_vector = np.arange(n_components_plot) * c_offset_step
        components_fft = 2.0 * np.abs(np.fft.rfft(components[:, list(range(n_components_plot))],  axis=0)) / \
                         j_ave._win_size - c_offset_vector.reshape(1, -1)
        p1 = ax1.plot(j_ave._fft_frequencies, components_fft[list(range(j_ave._fft_frequencies.shape[0])), :], linewidth=0.5)
        ax1.set_title(_title + 'Components')
        ax1.set_xlim(0, np.max(j_ave.frequencies_to_analyze) * 1.2)
        ax1.set_xlabel('Frequency [Hz]')
        ax1.set_ylabel('Relative amplitude')
        ax1.set_ylim(-n_components_plot * c_offset_step, c_offset_step)
        ax2 = ax1.twinx()
        ax2.set_ylim(ax1.get_ylim())
        ax2.set_yticks(-c_offset_vector)
        ax2.set_yticklabels(['C' + str(i) for i in range(n_components_plot)])
        ax2.spines["right"].set_position(("axes", - 0.1))
        fig.savefig(kwargs['path'] + sep + data_name +
                    _name_parameters +
                    '_Components_Freq' + '.png')

    # plot potential fields
    potential = np.zeros((len(h_t_square_test), len(h_t_square_test[0])))
    _f_tested = []
    for i, _htest in enumerate(h_t_square_test):
        for j, _tested_freq in enumerate(_htest):
            potential[i, j] = _tested_freq['spectral_magnitude']
            _f_tested.append(re.sub('\.', '_', str(np.round(_tested_freq['frequency_tested'], decimals=1))))
    for i in np.arange(potential.shape[1]):
        _title = 'Freq:' + _f_tested[i]
        eegpt.save_potential_map(potentials=potential[:, i],
                                 channels=channels,
                                 file_name=path + sep + data_name + '_scalp_dist' + _name_parameters + '_' + _f_tested[i],
                                 layout=layout,
                                 title=_title + '/' + 'Freq_' + _f_tested[i],
                                 vmin=-0.5,
                                 vmax=0.5)

    if not save_to_data_base:
        return
    # add frequency test to data base
    concatenated_stimuli = cat_dictionary_list(dict_list=stimulus_parameters)
    h_tests = generate_frequency_dict(j_averager=j_ave, channels=channels)
    # connect and add items to data base
    db = dataset.connect('sqlite:///' + data_base_path)
    table_subjects = db['subjects']
    table_subjects.create_column('name', sqlalchemy.String)
    table_subjects.create_column('anonymous_name', sqlalchemy.String)
    # check if subject is in DB
    result = table_subjects.find_one(name=measurement_info['subject'])
    if not result:
        _id_subject = table_subjects.insert({'name': measurement_info['subject']})
        table_subjects.update(dict(id=_id_subject, anonymous_name='S' + str(_id_subject)), ['id'])
    else:
        print('adding data to subject found in DB')
        _id_subject = result['id']
    # add experiment to table
    table_experiment = db['experiment']
    exp = {'type': experiment, 'id_subject': _id_subject}
    table_experiment.upsert(exp, list(exp.keys()))
    _id_experiment = table_experiment.find_one(type=experiment)['id']

    # add measurement info
    table_measurement_info = db['measurement_info']
    table_measurement_info.create_column('id_experiment', sqlalchemy.INTEGER)
    _row_measurement_info = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject},
                                 **measurement_info)
    table_measurement_info.upsert(_row_measurement_info, list(_row_measurement_info.keys()))
    # add stimulus_parameters to table
    table_stimuli = db['stimuli']
    table_stimuli.create_column('id_experiment', sqlalchemy.INTEGER)
    _row_stimuli = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject},
                        **concatenated_stimuli)
    table_stimuli.upsert(_row_stimuli, list(_row_stimuli.keys()))
    _id_stimuli = table_stimuli.find_one(**_row_stimuli)['id']

    # add recording processing to table
    table_recording = db['recording']
    _row_recording = {'id_stimuli': _id_stimuli,
                      'fs': bdf_reader.fs,
                      'system': 'biosemi',
                      'low_pass_filter': low_pass,
                      'high_pass_filter': high_pass,
                      'notch_filter': str(notch_filter),
                      'sf_freq': sf_freq,
                      'sf_join_frequencies': str(np.round(sf_join_frequencies, decimals=1)),
                      'sf_number_comp': n_components}
    table_recording.upsert(_row_recording, list(_row_recording.keys()))
    _id_recording = table_recording.find_one(**_row_recording)['id']

    # add frequency tests
    table_h_test = db['h_tsquare_test']
    for i, item in enumerate(h_tests):
        _row_test = dict({'id_stimuli': _id_stimuli, 'id_recording': _id_recording}, **item)
        table_h_test.upsert(_row_test, list(_row_test.keys()))

    # add time and freq data
    table_responses = db['responses']
    table_responses.create_column('id_stimuli', sqlalchemy.INTEGER)
    table_responses.create_column('id_recording', sqlalchemy.INTEGER)
    table_responses.create_column('domain', sqlalchemy.String)
    table_responses.create_column('type', sqlalchemy.String)
    table_responses.create_column('channel', sqlalchemy.String)
    table_responses.create_column('frequency-weighted', sqlalchemy.FLOAT)
    table_responses.create_column('x', sqlalchemy.LargeBinary)
    table_responses.create_column('y', sqlalchemy.LargeBinary)
    # save best channels spectra and wave forms to db
    for i, label in zip([pos_max_snr_mf], ['assr']):
        data_binary_x = io.BytesIO()
        np.save(data_binary_x, j_ave.time)
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.s_average[:, i])
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'time', 'type': 's_average_best_' + label,
                          'channel': channels[i]['label'],
                          'x': data_binary_x.read(), 'y': data_binary_y.read()}
        table_responses.upsert(_row_time_data, list(_row_time_data.keys()))
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.w_average[:, i])
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'time', 'type': 'w_average_best_' + label,
                          'channel': channels[i]['label'],
                          'x': data_binary_x.read(), 'y': data_binary_y.read()}
        table_responses.upsert(_row_time_data, list(_row_time_data.keys()))

    for i, label in zip([pos_max_snr_mf], ['assr']):
        data_binary_x = io.BytesIO()
        np.save(data_binary_x, j_ave._fft_frequencies)
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.s_fft[:, i])
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_freq_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'frequency', 'type': 's_average_best_' + label,
                          'channel': channels[i]['label'],
                          'x': data_binary_x.read(), 'y': data_binary_y.read()}
        table_responses.upsert(_row_freq_data, list(_row_freq_data.keys()))
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.w_fft[:, i])
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_freq_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'frequency', 'type': 'w_average_best_' + label,
                          'channel': channels[i]['label'],
                          'x': data_binary_x.read(), 'y': data_binary_y.read()}
        table_responses.upsert(_row_freq_data, list(_row_freq_data.keys()))

    # save averaged channel spectra at weighted at a given frequency bin
    for i in [bin_mod_freq]:
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.w_fft_spectral_weighted_ave_all_splits(i))
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'frequency', 'type': 'w_average',
                          'channel': 'w-averaged-bin',
                          'frequency-weighted': np.round(j_ave.frequencies_to_analyze[i], decimals=1),
                          'x': data_binary_x.read(), 'y': data_binary_y.read()}
        table_responses.upsert(_row_time_data, list(_row_time_data.keys()))

    # save spectral standard and weighted across electrode average averages
    data_binary_y = io.BytesIO()
    np.save(data_binary_y, j_ave.s_fft_spectral_ave_all_splits)
    data_binary_x.seek(0)
    data_binary_y.seek(0)
    _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                      'domain': 'frequency', 'type': 'w_average',
                      'channel': 's-averaged-bin',
                      'x': data_binary_x.read(), 'y': data_binary_y.read()}
    table_responses.upsert(_row_time_data, list(_row_time_data.keys()))

    data_binary_y = io.BytesIO()
    np.save(data_binary_y, j_ave.w_fft_spectral_ave_all_splits)
    data_binary_x.seek(0)
    data_binary_y.seek(0)
    _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                      'domain': 'frequency', 'type': 'w_average',
                      'channel': 'w-averaged',
                      'x': data_binary_x.read(), 'y': data_binary_y.read()}
    table_responses.upsert(_row_time_data, list(_row_time_data.keys()))

    # save individual channels if requested
    if save_single_recordings_to_db:
        for i in range(j_ave.splits):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, j_ave.time)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, j_ave.s_average[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                              'domain': 'time', 'type': 's_average',
                              'channel': channels[i]['label'],
                              'x': data_binary_x.read(), 'y': data_binary_y.read()}
            table_responses.upsert(_row_time_data, list(_row_time_data.keys()))
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, j_ave.w_average[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                              'domain': 'time', 'type': 'w_average',
                              'channel': channels[i]['label'],
                              'x': data_binary_x.read(), 'y': data_binary_y.read()}
            table_responses.upsert(_row_time_data, list(_row_time_data.keys()))

        for i in range(j_ave.splits):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, j_ave._fft_frequencies)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, j_ave.s_fft[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_freq_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                              'domain': 'frequency', 'type': 's_average',
                              'channel': channels[i]['label'],
                              'x': data_binary_x.read(), 'y': data_binary_y.read()}
            table_responses.upsert(_row_freq_data, list(_row_freq_data.keys()))
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, j_ave.w_fft[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_freq_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                              'domain': 'frequency', 'type': 'w_average',
                              'channel': channels[i]['label'],
                              'x': data_binary_x.read(), 'y': data_binary_y.read()}
            table_responses.upsert(_row_freq_data, list(_row_freq_data.keys()))


def get_biosemi_assr(bdf_reader=bioReader.BiosemiDataReader,
                     triggers={},
                     channels=[{}],
                     **kwargs):
    test_frequencies = kwargs.get('test_frequencies', [0])
    sf_freq = kwargs.get('sf_freq', True)
    sf_join_frequencies = kwargs.get('sf_join_frequencies', None)
    sf_number_comp = kwargs.get('sf_number_comp', None)

    epochs = bdf_reader.get_epochs(channels=channels, triggers=triggers, **kwargs)
    if sf_freq:
        c0, c1 = sf.nt_bias_fft(epochs, sf_join_frequencies / bdf_reader.fs)
    else:
        c0 = sf.nt_covariance(epochs)
        c1 = sf.nt_covariance(np.expand_dims(np.mean(epochs, axis=2), 2))

    todss, pwr0, pwr1 = sf.nt_dss0(c0, c1)
    if not sf_number_comp:
        p_ratio = pwr1 / pwr0
        pos = np.maximum(np.where(np.cumsum(p_ratio)/np.sum(p_ratio) >= 0.5)[0][0], 1)
        n_components = np.arange(pos)
    else:
        n_components = np.arange(sf_number_comp)

    z = sf.nt_mmat(epochs, todss)
    components = np.mean(z, axis=2)
    cov_1 = sf.nt_x_covariance(z, epochs)
    epochs = sf.nt_mmat(z[:, n_components, :], cov_1[n_components, :])

    j_ave = JAve.JAverager()
    j_ave.splits = epochs.shape[1]
    j_ave.fs = bdf_reader.fs
    j_ave.t_p_snr = np.arange(0, epochs.shape[0] / bdf_reader.fs, epochs.shape[0]/256.0 / bdf_reader.fs)
    j_ave.analysis_window = np.array([]) * 1e-3
    j_ave.time_offset = 0.0
    j_ave.alpha_level = 0.05
    j_ave.min_block_size = 5
    j_ave.plot_sweeps = False
    j_ave.frequencies_to_analyze = test_frequencies

    _n_sweep = 0
    _total_sweeps = epochs.shape[1] * epochs.shape[2]
    for i in range(epochs.shape[2]):
        for j in range(epochs.shape[1]):
            j_ave.add_sweep(epochs[:, j, i])
            _n_sweep += 1
            print(('current sweep:' + str(_n_sweep) + '/' + str(_total_sweeps)))
    return j_ave, epochs, len(n_components), components

