import numpy as np
from scipy import interpolate
from scipy.signal import windows, hilbert
from pyeeg.processing.tools.filters.eegFiltering import bandpass_fir_win, ols_filt_filt


def aep(fs=16384.0,
        time_length=1.0):
    """
    This function generates a typical auditory cortex waveform (as shown in  Human Auditory Evoked Potentials
    Terence W. Picton
    :param fs: sampling rate of the generated waveform (samples per seconds)
    :param time_length: desired time of the output
    :return: waveform and time vector
    """
    time = np.arange(0, time_length, 1/fs)
    # Terrace Picton peaks latencies (in ms)
    _peak_times = np.array([0.0,
                            0.75,
                            1.5, 2.0,  # I
                            2.5, 3.0,  # II
                            3.5, 4.0,  # III
                            4.5, 5.0,  # IV
                            6.0, 7.0,  # V
                            7.5, 9.0,  # VI-N0
                            13.0, 18.0,  # P0-Na
                            30.0, 40.0,  # Pa-Nb
                            50., 100,  # P1-N1
                            180.0, 300.,   # P2-N2
                            600.0,
                            1000.0
                            ])*1e-3

    # Amplitudes in uV
    _peak_amplitudes = np.array([0.0,
                                 0.0,
                                 0.15, 0.0,  # I
                                 0.1, -0.02,  # II
                                 0.2, 0.03,  # III
                                 0.4, 0.35,  # IV
                                 0.5, 0.1,  # V
                                 0.15, -0.4,  # VI-N0
                                 0.1, -0.7,  # P0-Na
                                 0.8, -0.7,  # Pa-Nb
                                 0.9, -2.0,  # P1-N1
                                 2.5, -1.0,  # P2-N2
                                 -0.2,
                                 0
                                 ])
    f = interpolate.interp1d(_peak_times, _peak_amplitudes, kind='linear', fill_value="extrapolate")
    _x = np.logspace(np.log10(time[1]), np.log10(time[-1]), num=75, endpoint=False)
    _y = f(_x)
    f2 = interpolate.interp1d(_x, _y, kind='quadratic', fill_value="extrapolate")
    ynew = f2(time).reshape(-1, 1)
    # plt.plot(_x, _y)
    # plt.plot(_peak_times, _peak_amplitudes, 'o')
    # plt.plot(time, ynew)
    # plt.show()
    return ynew, time


def eog(fs=16384.0,
        thau1: float = 0.05,
        thau2: float = 0.01,
        peak_amp: float = 100,
        event_duration=1.0):
    """
    This function generates a typical eye blink waveform with an amplitude of 300 uV
    :param fs: sampling rate of the generated waveform (samples per seconds)
    :return: waveform and time vector
    """
    event_time = np.arange(0, event_duration, 1/fs)
    # event = windows.flattop(event_time.size) + windows.general_gaussian(event_time.size, p=1.5, sig=5000)
    event = (np.exp(-event_time/thau1) - np.exp(-event_time/thau2))
    y_new = np.zeros(event_time.shape)
    y_new[0:event_time.size] = event
    # apply rise-fade window
    fade_in_out = np.ones(y_new.shape)
    fade_len = np.round(event_time.size // 20)
    fade_in_out[0: fade_len] = (np.sin(2 * np.pi * (1 / (4 * fade_len / fs)) *
                                          event_time[0: fade_len]) ** 2)
    fade_in_out[-fade_len:] = (np.sin(2 * np.pi * (1 / (4 * fade_len / fs)) *
                                      event_time[0: fade_len] + np.pi / 2) ** 2)
    y_new *= fade_in_out
    y_new *= peak_amp / np.abs(y_new).max()

    return y_new.reshape(-1, 1), event_time


def artifact_and_brain_envelope(fs: float = 16384.0,  # transducer delay in secs
                                stimulus_delay: float = 0.0,  # brainstem delay
                                brain_delay:  float = 0.113,  # brainstem delay
                                duration: float = 1,  # seconds
                                seed=None,  # random generator seed
                                leak_amplitude: float = 20.0,
                                brain_amplitude: float = 0.2
                                ):
    time = np.arange(0, duration, 1 / fs).reshape(-1, 1)

    # define stimulation waveform
    if seed is not None:
        np.random.seed(seed)
    stimulus_waveform = np.random.randn(time.size, 1)
    stimulus_waveform /= np.abs(stimulus_waveform).max()

    # apply rise-fade window
    fade_in_out = np.ones(stimulus_waveform.shape)
    fade_len = round(time.size // 20)
    fade_in_out[0: fade_len] = np.sin(2 * np.pi * (1 / (4 * fade_len / fs)) * time[0: fade_len]) ** 2
    fade_in_out[-fade_len:] = np.sin(2 * np.pi * (1 / (4 * fade_len / fs)) * time[0: fade_len] + np.pi / 2) ** 2
    stimulus_waveform *= fade_in_out

    # brain response follows signal envelop with a delay
    template_waveform = np.abs(hilbert(stimulus_waveform))
    _b_brain = bandpass_fir_win(high_pass=2, low_pass=80.0, fs=fs)
    # rectify  brain response
    template_waveform[template_waveform < 0] = 0

    # filter brain response
    template_waveform = ols_filt_filt(b=_b_brain, x=template_waveform)
    template_waveform *= fade_in_out

    # pad zeros to add stimulus and neural delay
    template_waveform = np.pad(template_waveform, ((int(fs * stimulus_delay) + int(fs * brain_delay), 0), (0, 0)),
                               'constant', constant_values=(0, 0))
    template_waveform /= np.abs(template_waveform).max()

    # apply delay to leaked stimulus (assuming system delay)
    leaked_stimulus = np.pad(stimulus_waveform, ((int(fs * stimulus_delay), int(fs * brain_delay)),
                                                 (0, 0)), 'constant', constant_values=(0, 0))

    # leaked artifact
    _b = bandpass_fir_win(None, 1000, fs=fs, ripple_db=60)
    leaked_stimulus = ols_filt_filt(b=_b, x=leaked_stimulus)

    # scale respective signals
    template_waveform *= brain_amplitude
    leaked_stimulus *= leak_amplitude
    return template_waveform, stimulus_waveform, leaked_stimulus
