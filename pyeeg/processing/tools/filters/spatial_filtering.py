import pyfftw
import multiprocessing
import numpy as np
import pyeeg.processing.tools.eeg_epoch_operators as eo
import scipy
__author__ = 'jundurraga'


def nt_dss0(c0: np.array = np.array([]),
            c1: np.array = np.array([]),
            **kwargs):
    keep0 = kwargs.get('keep0', [])
    keep1 = kwargs.get('keep1', 10.0 ** -9)
    topcs_1, evs_1 = nt_pcarot(c0, keep0)
    evs_1 = np.abs(evs_1)
    # truncate PCA series if needed
    if keep0:
        topcs_1 = topcs_1[:, 0:keep1-1]
        evs_1 = evs_1[0:keep1 - 1]

    if keep1:
        idx = np.where(evs_1 / np.max(evs_1) > keep1)[0]
        topcs_1 = topcs_1[:, idx]
        evs_1 = evs_1[idx]

    # apply PCA and whitening to the biased covariance
    n = np.diag(np.sqrt(1 / evs_1))
    c2 = n.T.dot(topcs_1.T).dot(c1).dot(topcs_1).dot(n)
    # matrix to convert PCA-whitened data to DSS
    topcs_2, evs2 = nt_pcarot(c2, keep0)

    # DSS matrix (raw data to normalized DSS)
    todss = topcs_1.dot(n).dot(topcs_2)
    n2 = np.diagonal(todss.T.dot(c0).dot(todss))
    todss = todss.dot(np.diag(1 / np.sqrt(n2)))  # adjust so that components are normalized

    # power per DSS component
    pwr0 = np.sqrt(np.sum((c0.T.dot(todss) ** 2), axis=0))  # unbiased
    pwr1 = np.sqrt(np.sum((c1.T.dot(todss) ** 2), axis=0))  # biased
    return todss, pwr0, pwr1


def nt_pcarot(cov: np.array = np.array([]),
              n: int = 0):
    eigen_val, eigen_vec = np.linalg.eig(cov)
    #eigen_val, eigen_vec = scipy.linalg.eig(cov)
    eigen_vec = np.real(eigen_vec)
    eigen_val = np.real(eigen_val)
    idx = np.argsort(eigen_val)[::-1]
    if n:
        idx = idx[0: n-1]
    return eigen_vec[:, idx], eigen_val[idx]


def nt_bias_fft(x:np.array = np.array([]),
                normalized_frequencies: np.array = np.array([])):
    if np.mod(x.shape[0], 2) == 0:
        fft_size = int((x.shape[0] / 2) + 1)
    else:
        fft_size = int((x.shape[0] + 1) / 2)

    freq = np.arange(fft_size) / float(x.shape[0])
    freq_filt = np.zeros(shape=(fft_size, 1), dtype=np.complex)
    for f_bin in eo.et_find_freq_bin(freq, normalized_frequencies):
        freq_filt[f_bin] = 1.0
    c0 = eo.et_covariance(x)
    c1 = np.zeros(c0.shape)
    for i in np.arange(x.shape[2]):
        fft = pyfftw.builders.rfft(x[:, :, i], overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                                   threads=multiprocessing.cpu_count())
        xfft = fft()
        z = xfft * freq_filt
        c1 = c1 + 2 * np.real(z.T.conj().dot(z))
    return c0, c1

