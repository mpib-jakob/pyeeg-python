import numpy as np
from pyeeg.definitions.channel_definitions import Domain, ChannelItem, ChannelType
from pyeeg.processing.tools.multiprocessing.multiprocessesing_filter import filt_data
import abc
import datetime
from pyeeg.io.eeg.reader import eeg_reader
from pyeeg.layouts import layouts
import astropy.units as u
from pyeeg.directories.tools import DirectoryPaths
from pyeeg.processing.events.event_tools import detect_events
import inspect
import pandas as pd
import io
from pyeeg.definitions.events import Events
from pathlib import Path
from os.path import sep
from pyeeg.processing.tools.template_generator.auditory_waveforms import eog
import gc


class DataNode(object):
    """
    Core data object used to keep data. During the creation, it requires the sampling rate and the actual data.
    Data is passed in a numpy array of n * m or n * m * t, where n represent samples, m channels, and t trials.
    Units are passed via unit kargs (default units is microVolts)
    """
    def __init__(self,
                 fs=None,
                 data: np.array = np.array([]),
                 domain=Domain.time,
                 time_offset=0.0,
                 n_fft=None,
                 layout: np.array([ChannelItem]) = None,
                 x_units=None,
                 y_units=None,
                 z_units=None,
                 process_history=[],
                 events: Events = None,
                 events_annotations: () = None,
                 paths: DirectoryPaths = None,
                 rn: np.array = None,
                 cum_rn: [np.array] = None,
                 snr: [np.array] = None,  # it may content snr for different ROIs
                 cum_snr: [np.array] = None,
                 s_var=None,
                 statistical_tests=None,
                 peak_times=None,
                 peak_frequency=None,
                 peak_to_peak_amplitudes=None,
                 peak_time_windows=None
                 ):
        # n * m * t data array
        # if data represent time or frequency
        # time offset from origin
        # layout
        # list used to store processing history that resulted in the current data
        self.fs = fs
        self._data = data
        self.domain = domain
        self.time_offset = time_offset
        self.layout = layout
        self.x_units = x_units
        self.y_units = y_units
        self.z_units = z_units
        self.events = events
        self.events_annotations = events_annotations
        self.process_history = process_history
        self.paths = paths
        # signal statistics
        self.rn = rn
        self.cum_rn = cum_rn
        self.snr = snr
        self.cum_snr = cum_snr
        self.s_var = s_var
        self.statistical_tests = statistical_tests
        # peak measures
        self.peak_times = peak_times
        self.peak_frequency = peak_frequency
        self.peak_to_peak_amplitudes = peak_to_peak_amplitudes
        self.peak_time_windows = peak_time_windows
        self.n_fft = n_fft
        if self.layout is None and data.size:
            self.layout = np.array([ChannelItem() for _ in range(self.data.shape[1])])  # default channel information
        self._x = None
        self._y = None

    def get_x(self):
        if self.domain == Domain.time:
            return np.arange(self.data.shape[0]) / self.fs - self.time_offset
        if self.domain == Domain.frequency:
            return np.arange(self.data.shape[0]) * self.fs / self.n_fft
        if self.domain == Domain.time_frequency:
            return self._x

    def set_x(self, value):
        self._x = value
    x = property(get_x, set_x)

    def get_y(self):
        return self._y

    def set_y(self, value):
        self._y = value

    y = property(get_y, set_y)

    def apply_layout(self, layout=None):
        for _s_i in layout.layout:
            for _i, _l in enumerate(self.layout):
                if _l.idx == _s_i.idx:
                    self.layout[_i] = _s_i
                    break

    def delete_channel_by_idx(self, idx: np.array = np.array([])):
        labels = '/'.join([_ch.label for _ch in self.layout[idx]])
        self.data = np.delete(self.data, idx, 1)
        self.layout = np.delete(self.layout, idx)
        print('removed channels: ' + ''.join(labels))

    def delete_channel_by_label(self, label=''):
        _idx = np.array([_i for _i, _lay in enumerate(self.layout) if _lay.label == label])
        self.delete_channel_by_idx(idx=_idx)

    def get_channel_idx_by_label(self, labels=['']):
        return np.unique([_idx for _idx, _channel in enumerate(self.layout) if _channel.label in labels])

    def get_channel_idx_by_type(self, channel_type=ChannelType.Event):
        return np.unique([_idx for _idx, _channel in enumerate(self.layout) if _channel.type == channel_type])

    def set_data(self, value=np.array):
        self._data = value

    def get_data(self):
        return self._data
    data = property(get_data, set_data)

    def get_max_snr_per_channel(self):
        if self.snr is not None:
            max_snr = np.nanmax(np.atleast_2d(self.snr), axis=0)
        else:
            max_snr = np.array([None] * self._data.shape[1])
        return np.atleast_1d(np.squeeze(max_snr))

    def get_max_s_var_per_channel(self):
        max_var = np.nanmax(self.s_var, axis=0)
        return np.atleast_1d(np.squeeze(max_var))

    def x_to_samples(self, value: np.array) -> np.array:
        out = np.array([])
        for _v in value:
            out = np.append(out, np.argmin(np.abs(self.x - np.array(_v)))).astype(np.int)
        return out

    def data_to_pandas(self):
        _row_test = []
        for i in range(self._data.shape[1]):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, self.x)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, self.data[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'domain': self.domain,
                              'channel': self.layout[i].label,
                              'x': data_binary_x.read(),
                              'y': data_binary_y.read(),
                              'snr': self.get_max_snr_per_channel()[i]}
            _row_test.append(_row_time_data)
        _data_pd = pd.DataFrame(_row_test)
        return _data_pd

    def append_new_channel(self, new_data: np.array = None, layout_label: str = ''):
        new_channel = ChannelItem(label=layout_label)
        self.layout = np.append(self.layout, new_channel)
        self.data = np.append(self.data, new_data, axis=1)
        self.rn = np.append(self.rn, np.nan)
        if self.snr is not None:
            for _i, _s in enumerate(self.snr):
                _s = np.append(_s, np.nan)
                self.snr[_i] = _s
        if self.s_var is not None:
            for _i, _s in enumerate(self.s_var):
                _s = np.append(_s, np.nan)
                self.s_var[_i] = _s


class InputOutputProcess(metaclass=abc.ABCMeta):
    """
    Core abstract input-output function. Each inherit class must provide an input data, a process function and and
    output data set
    """
    def __init__(self, input_process=object, keep_input_node=True, **kwargs):
        self.input_process = input_process  # Input data must be InputOutputProcess class
        self.function_args = kwargs
        self.input_node: DataNode = None
        self.output_node: DataNode = None
        self.process_parameters = kwargs  # dict with all the parameters used in process function
        self.keep_input_node = keep_input_node

    def transform_data(self):
        return 'core method to generate output data'

    def run(self):
        self.input_node = self.input_process.output_node
        callable_list = [_i for _i in dir(DataNode) if callable(getattr(DataNode, _i))]
        properties_list = [_i[0] for _i in inspect.getmembers(DataNode, lambda o: isinstance(o, property))]
        attributes = [a for a in dir(self.input_node) if not (a.startswith('__') or a.startswith('_')) and
                      a not in callable_list and a not in properties_list]
        pars = {_a: getattr(self.input_node, _a) for _a in attributes}
        self.output_node = DataNode(data=np.array([]),
                                    **pars
                                    )  # Output data must be Data class
        self.transform_data(**self.function_args)

        if not self.keep_input_node:
            if isinstance(self.input_node, DataNode):
                self.input_node.data = None
            self.input_node = None
            gc.collect()


class ReadInputData(InputOutputProcess):
    def __init__(self, file_path: str = None, channels_idx: np.array = np.array([]), ini_time=0,
                 end_time=np.Inf, layout_file_name: str = None, figures_subset_folder: str = '') -> InputOutputProcess:
        super(ReadInputData, self).__init__()
        self.reader = eeg_reader(file_path)
        self.file_path = file_path
        self.channels_idx = channels_idx
        self.ini_time = ini_time
        self.end_time = end_time
        self.output_node = None
        self.layout_file_name = layout_file_name
        self.figures_subset_folder = figures_subset_folder
        self.input_node = DataNode(fs=self.reader.fs,
                                   domain=Domain.time,
                                   layout=self.reader.default_layout,
                                   paths=DirectoryPaths(file_path=self.reader.file_name,
                                                        delete_all=False, delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        data, events, units, annotations = self.reader.get_data(channels_idx=self.channels_idx,
                                                                ini_time=self.ini_time,
                                                                end_time=self.end_time)

        # if data.shape[1] > self.channels_idx.size:
        #     self.events_idx = data.shape[1] - 1
        self.output_node = DataNode(data=data,
                                    fs=self.reader.fs,
                                    domain=Domain.time,
                                    layout=self.reader.default_layout,
                                    y_units=units,
                                    x_units=u.s
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.output_node.events = self.get_events(events)
        self.output_node.events_annotations = annotations

    def get_events(self, events):
        _events = detect_events(event_channel=events, fs=self.output_node.fs)
        _new_events = Events(events=np.array(_events))
        for i, _code in enumerate(np.unique(_new_events.get_events_code())):
            print('Event code:', _code, 'Number of events:', _new_events.get_events_code(code=_code).size)
        return _new_events


class MergeMultipleFiles(InputOutputProcess):
    def __init__(self, file_paths: [str] = None, channels_idx: np.array = np.array([]), layout_file_name: str = None,
                 figures_subset_folder: str = '') -> InputOutputProcess:
        super(MergeMultipleFiles, self).__init__()
        self.readers = np.array([])
        for _file in file_paths:
            self.readers = np.append(self.readers, eeg_reader(_file))
        self.file_paths = file_paths
        self.channels_idx = channels_idx
        self.ini_time = 0
        self.end_time = np.Inf
        self.output_node = None
        self.layout_file_name = layout_file_name
        self.figures_subset_folder = figures_subset_folder
        self.input_node = DataNode(fs=self.readers[0].fs,
                                   domain=Domain.time,
                                   layout=self.readers[0].default_layout,
                                   paths=DirectoryPaths(file_path=self.readers[0].file_name,
                                                        delete_all=False, delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        all_data = None
        all_events = None
        # sort readers by time
        _date_format = "%d.%m.%y/%H.%M.%S"
        dates = np.array([_reader._header['start_date'] + '/' + _reader._header['start_time'] for
                          _reader in self.readers])
        sorted_idx = np.argsort([datetime.datetime.strptime(_date, _date_format) for _date in dates])
        print(dates[sorted_idx])
        for _reader in self.readers[sorted_idx]:
            data, events, units = _reader.get_data(channels_idx=self.channels_idx,
                                                   ini_time=self.ini_time,
                                                   end_time=self.end_time)
            if all_data is not None:
                all_data = np.concatenate((all_data, data))
                all_events = np.concatenate((all_events, events))
            else:
                all_data = data
                all_events = events

        # if data.shape[1] > self.channels_idx.size:
        #     self.events_idx = data.shape[1] - 1
        self.output_node = DataNode(data=all_data,
                                    fs=self.readers[0].fs,
                                    domain=Domain.time,
                                    layout=self.readers[0].default_layout,
                                    y_units=units,
                                    x_units=u.s
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.get_events(all_events)

    def get_events(self, events):
        # _ch_idx = self.output_node.get_channel_idx_by_type(ChannelType.Event)
        # event_channel = self.output_node.data[:, _ch_idx]
        events = detect_events(event_channel=events, fs=self.output_node.fs)
        events = Events(events=np.array(events))
        for i, _code in enumerate(np.unique(events.get_events_code())):
            print('Event code:', _code, 'Number of events:', events.get_events_code(code=_code).size)
        self.output_node.events = events
        # self.output_node.delete_channel_by_idx(_ch_idx)


class GenerateInputData(InputOutputProcess):
    def __init__(self,
                 template_waveform: np.array = None,
                 stimulus_waveform: np.array = None,
                 fs: float = 16384.0,
                 alternating_polarity=False,
                 n_channels: int = None,
                 snr: float = 3,
                 line_noise_amplitude: float = 5,
                 include_non_stationary_noise_events=False,
                 noise_events_interval=25.0,
                 noise_events_duration=15.0,
                 noise_events_power_delta_db=6.0,
                 noise_seed=None,
                 include_eog_events=False,
                 event_times: np.array = None,
                 event_code: float = 1.0,
                 layout_file_name: str = None,
                 figures_path: str = None,
                 figures_subset_folder: str = '') -> InputOutputProcess:
        """
        This class allows to generate data that can be used without the need of having a bdf or edf file.
        This InputOutput process takes an input template and convolve it with delta at event times.
        The output will have as many channels as desired and the maximum snr will be given by snr.
        If requested, non-stationary noise events will be generated. The snr will be calculated relative to the
        stationary noise. The non-stationary events will vary around that stationary level by x dB as passed in
        noise_events_power_delta_db.
        The output events will be coded with the provided event code.
        :param template_waveform: a numpy array with the template waveform to be use as signal
        :param stimulus_waveform: a numpy array with the presented waveform. Useful to generate stimulus artifacts
        :param fs: the sampling rate of the template_waveform
        :param alternating_polarity: if true, stimulus_waveform is alternating in polarity for every epoch
        :param n_channels: number of channels to generate
        :param snr: signal-to-noise ratio, 0 < snr < inf. This will be used to set the amplitude of the stationary noise
        :param line_noise_amplitude: amplitude in uV of 50 Hz line noise
        :param include_non_stationary_noise_events: if True, non-stationary noise events will be included
        :param noise_events_interval: interval time [in seconds] between non-stationary events
        :param noise_events_duration: maximum duration of each non-stationary noise event
        :param noise_events_power_delta_db: the dB change of power in the noise noise relative to stationary noise power
        :param include_eog_events: if True, eog-like artifacts will be included
        :param event_times: numpy array with the location of the events. The wave
        :param event_code: desired event code to be assigned to time events
        :param layout_file_name: name of layout to be used. e.g. "biosemi32.lay"
        :param figures_path: path to save generated figures
        :param figures_subset_folder: string indicating a subfolder name in figures_path
        """
        super(GenerateInputData, self).__init__()
        self.template_waveform = template_waveform
        self.stimulus_waveform = stimulus_waveform
        self.alternating_polarity = alternating_polarity
        self.n_channels = n_channels
        self.snr = snr
        self.line_noise_amplitude = line_noise_amplitude
        self.fs = fs
        self.include_non_stationary_noise = include_non_stationary_noise_events
        self.noise_events_interval = noise_events_interval
        self.noise_events_duration = noise_events_duration
        self.noise_events_power_delta_db = noise_events_power_delta_db
        self.noise_seed = noise_seed
        self.event_times = event_times
        self.event_code = event_code
        self.include_eog_events = include_eog_events
        self.simulated_artifact = None
        self.simulated_neural_response = None
        self.simulated_artifact_response = None
        self.output_node = None
        self.layout_file_name = layout_file_name
        figures_path = figures_path if figures_path is not None else str(Path.home()) + '{:}'.format(sep +
                                                                                                     'pyeeg_python' +
                                                                                                     sep +
                                                                                                     'test' +
                                                                                                     sep +
                                                                                                     'figures')
        self.figures_subset_folder = figures_subset_folder

        _ch = []
        [_ch.append(ChannelItem(label='CH_{:}'.format(i), idx=i)) for i in range(n_channels)]
        layout = np.array(_ch)
        self.input_node = DataNode(fs=fs,
                                   domain=Domain.time,
                                   layout=layout,
                                   paths=DirectoryPaths(file_path=figures_path,
                                                        delete_all=False,
                                                        delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        # create synthetic data
        _events_idx = np.floor(self.event_times * self.fs).astype(np.int)
        events = np.zeros((int(self.event_times[-1] * self.fs) + 1, 1))
        events[_events_idx, :] = 1
        source = self.template_waveform
        # convolve source with dirac train to generate entire signal
        source = filt_data(input_data=source, b=events.flatten(), mode='full', onset_padding=False)
        artifacts = np.zeros(source.shape)
        if self.stimulus_waveform is not None:
            _events_source = events.copy()
            if self.alternating_polarity:
                _events_source[_events_idx[1:-1:2], :] = -1
            artifacts = filt_data(input_data=self.stimulus_waveform, b=_events_source.flatten(), mode='full',
                                  onset_padding=False)
        self.simulated_artifact = artifacts
        # mixing source matrix
        coeff = np.mod(np.arange(self.n_channels) + 1, self.n_channels//2 + 1) * 1 / (self.n_channels / 2)
        coeff = np.expand_dims(coeff, 0)

        # mixing artifact matrix
        art_coeff = np.roll(coeff, coeff.size // 4)

        # generates and convolve eye artifacts
        oeg_events = np.zeros(source.shape)
        if self.include_eog_events:
            if self.noise_seed is not None:
                np.random.seed(self.noise_seed)
            eog_time_events = np.sort(np.random.randint(0, source.size, int((source.size / self.fs)//4)) / self.fs)
            _oeg_events_idx = np.floor(eog_time_events * self.fs).astype(np.int)
            oeg_events[_oeg_events_idx, :] = 1
            _oeg_template, _ = eog(self.fs)
            oeg_events = filt_data(input_data=_oeg_template, b=oeg_events.flatten(), mode='full', onset_padding=False)
            oeg_events = oeg_events[0: source.size]

        neural_response = source * coeff
        artifact_response = artifacts * art_coeff
        self.simulated_neural_response = neural_response
        self.simulated_artifact_response = artifact_response
        s = neural_response + artifact_response
        eog_artifacts = oeg_events * coeff / 2
        # generate random noise
        if self.noise_seed is not None:
            np.random.seed(self.noise_seed)
        noise = np.random.normal(0, 0.1, size=(s.shape[0], self.n_channels))
        noise_ref_std = np.std(noise)
        if self.include_non_stationary_noise:
            # non-stationary noise (1 event very 20 seconds
            _new_noise_events = np.arange(0, source.shape[0] / self.fs, self.noise_events_interval)
            for _i, _ne in enumerate(_new_noise_events):
                _ini_pos = int(_ne * self.fs)
                if self.noise_seed is not None:
                    np.random.seed(self.noise_seed + 1)
                _noise_samples = np.random.randint(1, int(self.fs * self.noise_events_duration))
                _ini_pos = np.minimum(_ini_pos, source.shape[0])
                _end_pos = np.minimum(_ini_pos + _noise_samples - 1, source.shape[0])
                if self.noise_seed is not None:
                    np.random.seed(self.noise_seed + _i)
                noise[_ini_pos:_end_pos, :] += np.random.normal(0, 10**(self.noise_events_power_delta_db / 20) * 0.1,
                                                                size=(_end_pos - _ini_pos, self.n_channels))

        # generate line noise
        if self.noise_seed is not None:
            np.random.seed(self.noise_seed + 3)
        line_coeff = (1 + np.random.rand(1, coeff.shape[1])*0.0001) * self.line_noise_amplitude
        _line_signal = np.sin(2.0 * np.pi * 50.0 * np.arange(0, source.size) / self.fs).reshape(-1, 1)
        line_signal = _line_signal * line_coeff
        # generate data
        data = s + (np.std(source) / self.snr) * noise / noise_ref_std + line_signal + eog_artifacts

        events[_events_idx.astype(np.int)] = self.event_code
        units = u.uV

        self.output_node = DataNode(data=data,
                                    fs=self.fs,
                                    domain=Domain.time,
                                    layout=self.input_node.layout,
                                    y_units=units,
                                    x_units=u.s
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.get_events(events)
        if self.include_eog_events:
            oeg_events_with_noise = oeg_events + line_signal[:, 0].reshape(-1, 1)
            self.output_node.append_new_channel(new_data=oeg_events_with_noise,
                                                layout_label='EOG1')

    def get_events(self, events):
        # _ch_idx = self.output_node.get_channel_idx_by_type(ChannelType.Event)
        # event_channel = self.output_node.data[:, _ch_idx]
        events = detect_events(event_channel=events, fs=self.output_node.fs)
        events = Events(events=np.array(events))
        for i, _code in enumerate(np.unique(events.get_events_code())):
            print('Event code:', _code, 'Number of events:', events.get_events_code(code=_code).size)
        self.output_node.events = events
        # self.output_node.delete_channel_by_idx(_ch_idx)
