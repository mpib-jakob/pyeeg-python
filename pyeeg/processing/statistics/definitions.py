class HotellingTSquareFrequencyTest(object):
    def __init__(self,
                 frequency_tested=None,
                 frequency_bin=None,
                 df_1=None,
                 df_2=None,
                 t_square=None,
                 f=None,
                 p_value=None,
                 spectral_magnitude=None,
                 spectral_phase=None,
                 rn=None,
                 snr=None,
                 snr_db=None,
                 snr_critic_db=None,
                 snr_critic=None,
                 channel=None):
        self.frequency_tested = frequency_tested
        self.frequency_bin = frequency_bin
        self.df_1 = df_1
        self.df_2 = df_2
        self.t_square = t_square
        self.f = f
        self.p_value = p_value
        self.spectral_magnitude = spectral_magnitude
        self.spectral_phase = spectral_phase
        self.rn = rn
        self.snr = snr
        self.snr_db = snr_db
        self.snr_critic_db = snr_critic_db
        self.snr_critic = snr_critic
        self.channel = channel


class HotellingTSquareTest(object):
    def __init__(self,
                 df_1=None,
                 df_2=None,
                 t_square=None,
                 f=None,
                 f_critic = None,
                 p_value=None,
                 mean_amplitude=None,
                 rn=None,
                 snr=None,
                 snr_db=None,
                 snr_critic_db=None,
                 snr_critic=None,
                 channel=None):
        self.df_1 = df_1
        self.df_2 = df_2
        self.t_square = t_square
        self.f = f
        self.f_critic = f_critic
        self.p_value = p_value
        self.mean_amplitude = mean_amplitude
        self.rn = rn
        self.snr = snr
        self.snr_db = snr_db
        self.snr_critic_db = snr_critic_db
        self.snr_critic = snr_critic
        self.channel = channel


class FrequencyFTest(object):
    def __init__(self,
                 frequency_tested=None,
                 frequency_bin=None,
                 df_1=None,
                 df_2=None,
                 f=None,
                 p_value=None,
                 spectral_magnitude=None,
                 spectral_phase=None,
                 rn=None,
                 snr=None,
                 snr_db=None,
                 snr_critic_db=None,
                 snr_critic=None,
                 channel=None):
        self.frequency_tested = frequency_tested
        self.frequency_bin = frequency_bin
        self.df_1 = df_1
        self.df_2 = df_2
        self.f = f
        self.p_value = p_value
        self.spectral_magnitude = spectral_magnitude
        self.spectral_phase = spectral_phase
        self.rn = rn
        self.snr = snr
        self.snr_db = snr_db
        self.snr_critic_db = snr_critic_db
        self.snr_critic = snr_critic
        self.channel = channel


class PhaseLockingValueTest(object):
    def __init__(self,
                 plv=None,
                 df_1=None,
                 z_value=None,
                 z_critic = None,
                 p_value=None,
                 mean_phase=None,
                 channel=None,
                 frequency_tested=None,
                 rn=None):
        self.plv = plv
        self.df_1 = df_1
        self.z_value = z_value
        self.z_critic = z_critic
        self.p_value = p_value
        self.mean_phase = mean_phase
        self.channel = channel
        self.frequency_tested = frequency_tested
        self.rn = rn
