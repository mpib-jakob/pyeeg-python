# -*- coding: utf-8 -*-
"""
Created on Tue Dec 16 10:55:37 2014

@author: jundurraga-ucl
"""
from pyeeg.processing.tools import epochs_processing_tools as ept
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from pyeeg.processing.tools.eeg_epoch_operators import *
import pyeeg.processing.tools.eeg_epoch_operators as eo
from sklearn.decomposition import FastICA

# # create synthetic data
# fs = 1000.0
# nsamples = np.round(1 * fs).astype(np.int)
# nchans = 36
# ntrials = 130
# noise_dim = 36  # dimensionality of noise
# f1 = 40
#
# # Create 3 source signals
# source1 = np.expand_dims(np.sin(2 * np.pi * f1 * np.arange(nsamples) / fs), axis=1)
# source2 = np.expand_dims(np.sign(np.sin(1.5 * f1 * np.arange(nsamples) / fs)), axis=1)
# source3 = np.expand_dims(signal.sawtooth(0.5 * f1 * np.pi * np.arange(nsamples) / (2 * fs)), axis=1)
#
# # Concatenate into one matrix
# sources = np.hstack((source1, source2, source3))
#
# # Mixing matrix
# np.random.seed(1)
# A = (np.random.random((nchans, sources.shape[1])) - .5) / 0.5
#
# # Mixed signal matrix
# M = np.dot(sources, A.T)
#
# ## SD
# s_std = np.std(sources, axis=0)
#
# X = np.tile(np.expand_dims(M, axis=2), (1, 1, ntrials))
#
# desired_snr = 15.0
# ini_std = 10.0 ** (-desired_snr / 20.0) * s_std * ntrials ** 0.5
# theoretical_rn = ini_std / ntrials ** 0.5
#
# np.random.seed(1)
# noise = np.random.normal(0, ini_std[0], size=(nsamples, nchans, ntrials))
# X[:, 0] = X[:, 0] * 0.5
# data = noise + X
#
# w_ave, w, rn, cumulative_rn, snr, cumulative_snr, s_var, w_fft, nk = ept.et_mean(epochs=data,
#                                                                                  block_size=10,
#                                                                                  samples_distance=10)
# across_channels_ave, total_rn, total_snr, t_s_var = \
#     ept.et_snr_weighted_mean(averaged_epochs=w_ave, rn=rn, snr=np.max(snr, axis=0))
#
# s_components, s_unmixing, s_mixing, s_pwr, whitening_m = ept.et_ica_epochs(data=w_ave,
#                                                                            tol=1e-4,
#                                                                            iterations=2000)
# # same as s_components
# p_data = s_unmixing.dot(np.dot(whitening_m, w_ave.T)).T
#
# # keep components explaining 80% of total variance
# cumpower = np.cumsum(s_pwr) / np.sum(s_pwr)
# n_idxs = np.argwhere(cumpower <= 0.8)
#
# # clean data
# w_c = np.zeros(s_mixing.shape)
# w_c[:, n_idxs] = 1
# s_clean = s_mixing * w_c
# clean_data = s_clean.dot(s_components.T).T
# clean_data2 = s_mixing.dot(p_data.T).T

# # Plot
# plt.figure()
# plt.subplot(4, 1, 1)
# plt.plot(sources)
# plt.subplot(4, 1, 2)
# plt.plot(w_ave + np.arange(0, w_ave.shape[1])*w_ave.max())
# plt.subplot(4, 1, 3)
# plt.plot(clean_data + np.arange(0, clean_data.shape[1]) * clean_data.max())
# plt.subplot(4, 1, 4)
# plt.plot(p_data + np.arange(0, p_data.shape[1])*p_data.max())
#
# plt.figure()
# plt.plot(s_components + np.arange(0, s_components.shape[1]) * s_components.max())
# plt.show()

# ICA in 3 dimensions ##################
# Synthesise data
fs = 44100.0
nsamples = int(np.round(1 * fs))
nchans = 36
ntrials = 100
noise_dim = 36  # dimensionality of noise
f1 = 40
source = np.expand_dims(np.sin(2 * np.pi * f1 * np.arange(nsamples) / fs + 3 * np.pi / 2), axis=1)
coeff = np.arange(nchans / 2) * 0.5 / (nchans / 2)
coeff = np.expand_dims(np.hstack((coeff, -coeff)), 0)

# s = source * np.random.normal(0, 0.1, size=(1, nchans))
s = source * coeff
s = np.tile(np.expand_dims(s, axis=2), (1, 1, ntrials))
SNR = 10
noise = np.random.normal(0, 0.1, size=(nsamples, nchans, ntrials))
n_source = SNR * s / np.std(s)
data = noise / np.std(noise) + n_source

unf_data = et_unfold(data)

s_components, s_unmixing, s_mixing, s_pwr, whitening_m = ept.et_ica_epochs(data=unf_data,
                                                                           tol=1e-4,
                                                                           iterations=10)

components = et_fold(s_components, data.shape[0])
scomps = np.mean(components, axis=2)
plt.plot(scomps + np.arange(0, scomps.shape[1]) * scomps.max())
plt.show()

plt.show()
