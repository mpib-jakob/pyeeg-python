__author__ = 'jundurraga'
import dataset
import sqlalchemy
import numpy as np
import io
db = dataset.connect('sqlite:///test4.sqlite')
subjects = db['Subjects']
data = np.random.random((1, 100000))
data_bin = io.BytesIO()
np.save(data_bin, data)
data_bin.seek(0)
subjects.create_column('data', sqlalchemy.LargeBinary)
subjects.insert(dict(data=data_bin.read(), name='John Doe', age=46, country='China'))
users = db['Subjects'].all()
for u in users:
    if u['data'] != '':
        data_db_bin = io.BytesIO(u['data'])
        a = np.load(data_db_bin)
        print(a)
print(users)

