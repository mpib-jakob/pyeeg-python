import matplotlib.pyplot as plt
import pyeeg.processing.tools.filters.spatial_filtering as sf
from pyeeg.processing.tools.eeg_epoch_operators import et_covariance, et_x_covariance, et_mmat
from pyeeg.processing.tools.epochs_processing_tools import et_weighted_mean
import numpy as np
__author__ = 'jundurraga-ucl'

# create synthetic data
fs = 44100.0
nsamples = int(np.round(1 * fs))
nchans = 36
ntrials = 100
noise_dim = 36  # dimensionality of noise
f1 = 40
source = np.expand_dims(np.sin(2 * np.pi * f1 * np.arange(nsamples) / fs), axis=1)
coeff = np.arange(nchans/2) * 0.5 / (nchans / 2)
coeff = np.expand_dims(np.hstack((coeff, -coeff)), 0)

s = source * coeff
s = np.tile(np.expand_dims(s, axis=2), (1, 1, ntrials))
SNR = 0.1
noise = np.random.normal(0, 0.1, size=(nsamples, nchans, ntrials))
# noise[1000:12000, :, 30:70] = 20.0 * noise[1000:12000, :, 30:70]
n_source = SNR * s / np.std(s)
data = noise / np.std(noise) + n_source

w_data, _, _, _, _, _, _, _, _ = et_weighted_mean(data, block_size=2)
c0 = et_covariance(data)
c1 = et_covariance(np.expand_dims(np.mean(data, axis=2), 2))
todss, pwr0, pwr1 = sf.nt_dss0(c0, c1)
z = et_mmat(data, todss)
n_components = np.arange(1)
cov_1 = et_x_covariance(z, data)
data_clean = et_mmat(z[:, n_components, :], cov_1[n_components, :])

ax = plt.subplot(131)
ax.plot(np.mean(n_source, axis=2))
ax = plt.subplot(132)
ax.plot(np.mean(data, axis=2))
ax = plt.subplot(133)
ax.plot(np.mean(data_clean, axis=2))
plt.show()