import numpy as np


class SingleEvent(object):
    def __init__(self,
                 code=None,
                 time_pos=None,
                 dur=None):
        """
        Class for a single (trigger) event, which is characterized by having a numerical identifier (code), an associated
        timepoint [s], and a duration [s].
        Parameters
        ----------
        code: Trigger event code (usually some number)
        time_pos: Time in seconds of trigger event
        dur: Duration of trigger event in seconds
        """
        self.code = code
        self.time_pos = time_pos
        self.dur = dur

    def __repr__(self):
         return(f'{self.__class__.__name__}('
                f'{self.code!r}, {self.time_pos!r}, {self.dur!r}')

    def __str__(self):
        return 'Single event at time {} s with code {} and duration {} s'.format(self.time_pos, self.code, self.dur)


class Events(object):
    def __init__(self, events: np.array([SingleEvent]) = None):
        """
        Defines event class
        :param events: raw events array
        """
        self.events = events

    def __repr__(self):
         return(f'{self.__class__.__name__}('
                f'{self.events.shape!r}')

    def __str__(self):
        return 'Event array of size {}'.format(self.events.shape)

    def get_events(self, code=None):
        if code is None:
            return np.array([_ev for _ev in self.events])
        else:
            return np.array([_ev for _ev in self.events if _ev.code == code])

    def get_events_index(self, code=None, fs=None):
        if code is None:
            return np.array([int(_ev.time_pos * fs) for _ev in self.events])
        else:
            return np.array([int(_ev.time_pos * fs) for _ev in self.events if _ev.code == code])

    def get_events_time(self, code=None):
        if code is None:
            return np.array([_ev.time_pos for _ev in self.events])
        else:
            return np.array([_ev.time_pos for _ev in self.events if _ev.code == code])

    def get_events_duration(self, code=None):
        if code is None:
            return np.array([_ev.duration for _ev in self.events])
        else:
            return np.array([_ev.duration for _ev in self.events if _ev.code == code])

    def get_events_code(self, code=None):
        if code is None:
            return np.array([_ev.code for _ev in self.events])
        else:
            return np.array([_ev.code for _ev in self.events if _ev.code == code])

    def interpolate_events(self, scaling_factor=1.0, code=None):
        """
        This function interpolates time events between consecutive event. The time position of new events is defined
        by the scaling factor. For example, an scaling_factor of 2.0 will add a new trigger event exactly in the middle
        time between two events
        :param scaling_factor: a float that determine the number of new events that will be generated
        :param code: event code to be interpolated
        :return: new an Events class containing the new interpolated events
        """
        _events = self.get_events(code=code)
        _times = np.array([_ev.time_pos for _ev in _events])
        _durations = np.array([_ev.dur for _ev in _events])
        _codes = np.array([_ev.code for _ev in _events])
        _times_intervals = np.diff(_times)
        _times_intervals = np.append(_times_intervals, np.mean(_times_intervals))
        _new_events = []
        for _ini_time, _length, _dur, _code in zip(_times, _times_intervals, _durations, _codes):
            _new_times = np.arange(_ini_time, _ini_time + _length, _length / scaling_factor)
            [_new_events.append(SingleEvent(time_pos=_t, dur=_dur, code=_code)) for _t in _new_times]
        return Events(events=np.array(_new_events))
