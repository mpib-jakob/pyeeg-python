from pyeeg.processing.pipe.pype_line_definitions import *
from pyeeg.processing.pipe.definitions import GenerateInputData
from pyeeg.processing.tools.roi.definitions import TimeROI
from pyeeg.processing.tools.template_generator.auditory_waveforms import aep
from pyeeg.io.storage.data_storage_tools import *
import os


def my_pipe():
    tw = np.array([
        TimePeakWindow(ini_time=4e-3, end_time=6e-3, label='V', positive_peak=True),
        TimePeakWindow(ini_ref='V', end_time=12e-3, label='N_V', positive_peak=False),
        TimePeakWindow(ini_time=4e-3, end_ref='V', label='N_III', positive_peak=False),
        TimePeakWindow(ini_time=3e-3, end_ref='N_III', label='III', positive_peak=True),
        TimePeakWindow(ini_time=2e-3, end_ref='III', label='N_II', positive_peak=False),
        TimePeakWindow(ini_time=2e-3, end_ref='N_II', label='II', positive_peak=True),
        TimePeakWindow(ini_time=1e-3, end_ref='II', label='N_I', positive_peak=False),
        TimePeakWindow(ini_time=1e-3, end_ref='N_I', label='I', positive_peak=True),
    ])

    pm = np.array([
        PeakToPeakMeasure(ini_peak='I', end_peak='N_I'),
        PeakToPeakMeasure(ini_peak='II', end_peak='N_II'),
        PeakToPeakMeasure(ini_peak='III', end_peak='N_III'),
        PeakToPeakMeasure(ini_peak='V', end_peak='N_V'),
        PeakToPeakMeasure(ini_peak='III', end_peak='V')])

    roi_windows = np.array([TimeROI(ini_time=1e-3, end_time=10.0e-3, measure="snr", label="abr_snr")])
    fs = 16384.0
    template_waveform, _ = aep(fs=fs)
    template_waveform = template_waveform[np.arange(0, int(0.02 * fs))]
    n_channels = 32
    event_times = np.arange(0, 360.0, 1./11)
    reader = GenerateInputData(template_waveform=template_waveform,
                               fs=fs,
                               n_channels=n_channels,
                               snr=0.2,
                               event_times=event_times,
                               event_code=1.0,
                               layout_file_name='biosemi32.lay',
                               figures_subset_folder='abr_test')
    reader.run()

    pipe_line = PipePool()
    pipe_line.append(ReferenceData(reader, reference_channels=['Cz'], invert_polarity=True),
                     name='referenced')
    pipe_line.append(AutoRemoveBadChannels(pipe_line.get_process('referenced')), name='channel_cleaned')
    pipe_line.append(FilterData(pipe_line.get_process('channel_cleaned'), high_pass=100.0, low_pass=3000.0),
                     name='time_filtered_data')
    pipe_line.append(EpochData(pipe_line.get_process('time_filtered_data'),
                               event_code=1.0,
                               base_line_correction=False),
                     name='time_epochs')
    pipe_line.append(CreateAndApplySpatialFilter(pipe_line.get_process('time_epochs'),
                                                 plot_x_lim=[0, 0.012]),
                     name='dss_time_epochs')
    pipe_line.append(HotellingT2Test(pipe_line.get_process('time_epochs'),
                                     roi_windows=roi_windows,
                                     block_time=1e-3),
                     name='ht2')
    pipe_line.append(AverageEpochs(pipe_line.get_process('time_epochs'), roi_windows=roi_windows),
                     name='time_average')
    # we up-sample to improve time peak detection
    pipe_line.append(ReSampling(pipe_line.get_process('time_average'), new_sampling_rate=fs * 2),
                     name='up_sampled')

    pipe_line.append(PeakDetectionTimeDomain(pipe_line.get_process('up_sampled'),
                                             time_peak_windows=tw,
                                             peak_to_peak_measures=pm))
    pipe_line.append(PlotTopographicMap(pipe_line[-1].process, plot_x_lim=[0, 0.012],
                                        plot_y_lim=[-0.5, 0.5]))

    pipe_line.run()

    time_measures = pipe_line.get_process('PeakDetectionTimeDomain')
    time_table = PandasDataTable(table_name='time_peaks',
                                 pandas_df=time_measures.output_node.peak_times)
    amps_table = PandasDataTable(table_name='amplitudes',
                                 pandas_df=time_measures.output_node.peak_to_peak_amplitudes)

    time_waveforms = pipe_line.get_process('up_sampled')
    waveform_table = PandasDataTable(table_name='time_waveforms',
                                     pandas_df=time_waveforms.output_node.data_to_pandas())

    ht2_tests = pipe_line.get_process('ht2')
    h_test_table = PandasDataTable(table_name='h_t2_test',
                                   pandas_df=ht2_tests.output_node.statistical_tests)

    # now we save our data to a database
    subject_info = SubjectInformation(subject_id='Test_Subject')
    measurement_info = MeasurementInformation(
        date='Today',
        experiment='sim')

    _parameters = {'Type': 'ABR'}
    data_base_path = reader.input_node.paths.file_directory + os.sep + 'abr_test_data.sqlite'
    store_data(data_base_path=data_base_path,
               subject_info=subject_info,
               measurement_info=measurement_info,
               recording_info={'recording_device': 'dummy_device'},
               stimuli_info=_parameters,
               pandas_df=[time_table, amps_table, waveform_table, h_test_table])


if __name__ == "__main__":
    my_pipe()
